create database vallapureddy;
use vallapureddy;
CREATE TABLE customers (
    customer_id INT AUTO_INCREMENT PRIMARY KEY,
    customer_name VARCHAR(100),
    email_address VARCHAR(100),
    city VARCHAR(50),
    phone_number VARCHAR(15),
    join_date DATE
);
INSERT INTO customers (customer_name, email_address, city, phone_number, join_date)
VALUES ('Surekha', 'surekhavallapureddy@gmail.com', 'Hyderabad', '9848137825', '2024-01-15'),
('Surya', 'v.jaisurya999@gmail.com', 'Chennai', '9666331127', '2019-05-23'),
('Pavani', 'pavanitummuri@gmail.com', 'Pune', '9704620848', '2021-08-09');
SELECT * FROM customers;
SELECT customer_name, email_address
FROM customers
WHERE city = 'Hyderabad';