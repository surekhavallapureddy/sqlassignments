create database surekhawiprodb;
use surekhawiprodb;
CREATE TABLE orders (
order_id INT AUTO_INCREMENT PRIMARY KEY,customer_id INT,order_date DATE,amount DECIMAL(10, 2)
);
desc orders;
CREATE TABLE products (product_id INT AUTO_INCREMENT PRIMARY KEY,product_name VARCHAR(100),category VARCHAR(50),
price DECIMAL(10, 2));
select * FROM orders;
start  TRANSACTION;
INSERT INTO orders (customer_id, order_date, amount)
VALUES (4, '2024-05-19', 300.00);
COMMIT;
set sql_safe_updates=0;
UPDATE products
SET price = price * 1.1
WHERE category = 'Electrical';
ROLLBACK;


