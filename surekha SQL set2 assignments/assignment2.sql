create database surekha12;
use surekha12;
CREATE TABLE customers (
    customer_id INT AUTO_INCREMENT PRIMARY KEY,
    customer_name VARCHAR(100),
    email_address VARCHAR(100),
    city VARCHAR(50),
    phone_number VARCHAR(15),
    join_date DATE
);
desc customers;
CREATE TABLE orders (
    order_id INT AUTO_INCREMENT PRIMARY KEY,
    customer_id INT,
    order_date DATE,
    amount DECIMAL(10, 2),
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);
desc orders;
INSERT INTO customers (customer_name, email_address, city, phone_number, join_date)
VALUES ('Surekha', 'surekhavallapureddy@gmail.com', 'Hyderabad', '9848137825', '2024-01-15'),
('Surya', 'v.jaisurya999@gmail.com', 'Chennai', '9666331127', '2019-05-23'),
('Pavani', 'pavanitummuri@gmail.com', 'Pune', '9704620848', '2021-08-09');
SELECT * FROM customers;
INSERT INTO orders (customer_id, order_date, amount)
VALUES (1, '2023-01-10', 250.50),(2, '2023-02-15', 450.00),
(1, '2023-03-20', 150.75);
SELECT * FROM orders;
SELECT c.customer_name, c.email_address, o.order_id, o.order_date, o.amount
FROM customers c INNER JOIN orders o ON c.customer_id = o.customer_id
WHERE c.city = 'Hyderabad';

SELECT c.customer_name, c.email_address, o.order_id, o.order_date, o.amount
FROM customers c LEFT JOIN orders o ON c.customer_id = o.customer_id;


