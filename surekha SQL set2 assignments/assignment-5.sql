create database surekha1wiprodb;
use surekha1wiprodb;
CREATE TABLE orders (order_id INT AUTO_INCREMENT PRIMARY KEY,customer_id INT,
order_date DATE,amount DECIMAL(10, 2));
START TRANSACTION;
INSERT INTO orders (customer_id, order_date, amount)VALUES (1, '2024-05-20', 150.00);
SAVEPOINT savepoint1;
INSERT INTO orders (customer_id, order_date, amount)VALUES (2, '2024-05-21', 200.00);
SAVEPOINT savepoint2;
INSERT INTO orders (customer_id, order_date, amount)VALUES (3, '2024-05-22', 250.00);
ROLLBACK TO SAVEPOINT savepoint2;
COMMIT;