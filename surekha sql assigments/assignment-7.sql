create database assignment7_db;
use assignment7_db;
CREATE TABLE books (
    book_id INT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL,
    published_year INT,
    genre VARCHAR(100),
    ISBN VARCHAR(13) UNIQUE NOT NULL);
CREATE TABLE members (
    member_id INT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    phone VARCHAR(20),
    membership_start DATE NOT NULL,
    membership_end DATE);
CREATE TABLE loans (
    loan_id INT PRIMARY KEY,
    book_id INT,
    member_id INT,
    loan_date DATE NOT NULL,
    return_date DATE,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (member_id) REFERENCES members(member_id));
show tables from assignment7_db;
desc books;
desc members;
desc loans;
select * from books;
INSERT INTO books (book_id, title, author, published_year, genre, ISBN)
VALUES (101, 'The Great Gatsby', 'F. Scott Fitzgerald', 1925, 'Fiction', 9666332227),
(102, 'machines', 'nagarkothari', 2017, 'EEE', '9689056723'),
(103,'controlsystems','chakrabarthy',2000,'circuits','9030372918');
update books
set published_year=2024
where book_id=101;

DELETE FROM books
WHERE published_year = 2000;

