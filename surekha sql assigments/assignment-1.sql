
create database wiprodb;
use wiprodb;
CREATE TABLE Product (
    ProductID INT PRIMARY KEY,
    ProductName VARCHAR(255),
    Version VARCHAR(50),
    Description TEXT
);
CREATE TABLE Customer (
    CustomerID INT PRIMARY KEY,
    Name VARCHAR(100),
    Email VARCHAR(100),
    Address VARCHAR(255)
);
CREATE TABLE Purchase (
    PurchaseID INT PRIMARY KEY,
    ProductID INT,
    CustomerID INT,
    PurchaseDate DATE,
    FOREIGN KEY (ProductID) REFERENCES Product(ProductID),
    FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
);
CREATE TABLE SupportTicket (
    TicketID INT PRIMARY KEY,
    ProductID INT,
    CustomerID INT,
    IssueDescription TEXT,
    DateOpened DATE,
    DateClosed DATE,
    FOREIGN KEY (ProductID) REFERENCES Product(ProductID),
    FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
);