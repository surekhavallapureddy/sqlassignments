create database assignment5;
use assignment5;
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    department_id INT);
desc employees;
INSERT INTO employees (employee_id, first_name, last_name, department_id)
VALUES (1, 'surekha', 'Vallapureddy', 5),
       (2, 'surya', 'Pati', 2),
       (3, 'sai', 'Gariganti', 1),
       (4, 'giri', 'Sanaboina', 3),
       (5, 'likitha', 'Baswa', 2);
desc employees;
EXPLAIN SELECT * FROM employees WHERE department_id = 2;
CREATE INDEX idx_department_id ON employees(department_id);
desc employees;
DROP INDEX idx_department_id ON employees;
EXPLAIN SELECT * FROM employees WHERE department_id = 2;
