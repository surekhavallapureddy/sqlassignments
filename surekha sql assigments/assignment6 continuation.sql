show databases;
use assignment6;
show tables from assignment6;
select * from employees;
desc employees;
INSERT INTO employees (first_name, last_name, position, date_of_birth)
VALUES ('Surekha', 'Vallapureddy', 'Software Engineer', '2002-01-12'),
('JaiSurya', 'Yarramsetti', 'Project Manager', '2000-04-27');
SHOW GRANTS FOR 'surekha2'@'localhost';
set sql_safe_updates=0;
UPDATE employees
SET position = 'Senior Software Engineer'
WHERE first_name = 'Surekha';
