create database assignment4_db;
use assignment4_db;
CREATE TABLE Authors (AuthorID INT PRIMARY KEY,
    FirstName VARCHAR(20) NOT NULL,
    LastName VARCHAR(20) NOT NULL,
    CONSTRAINT UC_Author UNIQUE (FirstName, LastName));
desc authors;
CREATE TABLE Publishers (
    PublisherID INT PRIMARY KEY,
    PublisherName VARCHAR(50) NOT NULL,
    CONSTRAINT UC_PublisherName UNIQUE (PublisherName));
desc publishers;
CREATE TABLE Members (
    MemberID INT PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    Email VARCHAR(100) UNIQUE,
    Address VARCHAR(255) NOT NULL,
    Phone VARCHAR(20) NOT NULL);
desc members;
ALTER TABLE Publishers ADD COLUMN Country VARCHAR(100);
DROP TABLE IF EXISTS Publishers;
show tables from assignment4_db;




