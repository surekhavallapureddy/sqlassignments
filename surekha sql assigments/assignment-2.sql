create database assignment2;
use assignment2;
CREATE TABLE Books (
    book_id INT PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    author VARCHAR(50) NOT NULL,
    publication_year INT CHECK (publication_year > 0),
    genre VARCHAR(50),
    ISBN VARCHAR(20) UNIQUE NOT NULL);
    desc books;

CREATE TABLE Members (
    member_id INT PRIMARY KEY,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    email VARCHAR(50),
    phone_number VARCHAR(10));

CREATE TABLE CheckoutRecords (
    record_id INT PRIMARY KEY,
    book_id INT,
    member_id INT,
    checkout_date DATE,
    due_date DATE,
    return_date DATE,
    FOREIGN KEY (book_id) REFERENCES Books(book_id),
    FOREIGN KEY (member_id) REFERENCES Members(member_id));
show tables from assignment2;
desc books;
desc checkoutrecords;
desc members;
