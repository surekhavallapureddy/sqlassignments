create database assignment6;
use assignment6;
CREATE TABLE employees (
    employee_id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    position VARCHAR(50),
    salary DECIMAL(10, 2)
);
desc employees;
CREATE TABLE departments (
    department_id INT AUTO_INCREMENT PRIMARY KEY,
    department_name VARCHAR(50)
);
desc departments;
show tables from assignment6;
CREATE USER 'surekha2'@'localhost' IDENTIFIED BY 'surekha';
GRANT SELECT, INSERT, UPDATE ON assignment6.* TO 'surekha2'@'localhost';
REVOKE select,update ON assignment6.* FROM 'surekha2'@'localhost';
show grants for 'surekha2'@'localhost';
select * from employees;
ALTER TABLE employees ADD COLUMN date_of_birth DATE;
ALTER TABLE employees DROP COLUMN salary;